from MovementType import MovementType

class Piece():
    
    color = 'color_undefined'
    
    def __init__(self, color):
        assert color.lower() in ('w', 'white', 'b', 'black'), 'Piece color:' + color + ' is not a valid color.'
        if color.lower() in ('w', 'white'): self.color = 'white'
        elif color.lower() in ('b', 'black'): self.color = 'black'
        else: self.color = 'color_undefined:' + color
    
    def isWhite(self):
        if self.color in ('w', 'white'): return True
        else: return False

    #overwrite
    def getMovementType(self):
        #this method should return newly created instance of MovementType class, not same ones every time
        #otherwise when BoardManager flips MovementType.allowedCoordinates[i].moveCondition.x and MovementType.allowedCoordinates[i].moveCondition.y
        #to view them from black perspective
        #that would change x and y permanently so next time BoardManager gets MoveCondition-s for same pieces from MovementType.conditions
        #some MoveCondition-s could have incorrect(still flipped) MoveCondition.x and MoveCondition.y
        return None

    #returns True if this piece in position startX, startY could in one move reach squareX, squareY,
    #while not taking into account if pieces are on its path and if piece is a jumper.
    #returns False if this piece is standing on squareX, squareY
    #1-indexed
    def isAttacking(self, startX, startY, squareX, squareY):

        if startX == squareX and startY == squareY: return False

        reachableInOneMove = self.getMovementType().allowedPositions(startX, startY, self.isWhite())

        if (squareX, squareY) in reachableInOneMove: return True
        return False
    
    #__str__ returns char representation of the piece in lower case
    #depending on return from isWhite function char representation
    #will be displayed lower case (if black) or upper case (if white)
    def __str__(self): return 'generic type'
    
    
class EmptySquare():

    def __str__(self): return '_'
    


class Pawn(Piece):
    
    def __init__(self, color): super().__init__(color)#super(color)
    
    def getMovementType(self):
        return MovementType('vertical_one_square_foreward_on_empty_square,diagonal_capture,double_move_from_second_rank,en_passant', 1)

    #pawn has to overwrite generic isAttacking because pawn can move foreword but only attacks to the side
    def isAttacking(self, startX, startY, squareX, squareY):

        if startX == squareX and startY == squareY: return False

        reachableInOneMove = self.getMovementType().allowedPositions(startX, startY, self.isWhite())

        reachableInOneMoveAttacks = []

        #filter out forward moves from allowedPositions, leaving only side captures
        for move in reachableInOneMove:
            if startX == move[0]:#if same x before and after pawn move
                pass
            else: reachableInOneMoveAttacks.append( (move[0], move[1]) )

        if (squareX, squareY) in reachableInOneMoveAttacks: return True
        return False

    def __str__(self):
        return 'p'

class Rook(Piece):
    
    def __init__(self, color): super().__init__(color)#super(color)
    
    def getMovementType(self):
        return MovementType('straight', 8)

    def __str__(self):
        return 'r'

class Knight(Piece):
    
    def __init__(self, color): super().__init__(color)#super(color)
    
    def getMovementType(self):
        return MovementType('knight_move', 0, True)

    def __str__(self):
        return 'n'

class Bishop(Piece):
    
    def __init__(self, color): super().__init__(color)#super(color)
    
    def getMovementType(self):
        return MovementType('diagonal', 8)

    def __str__(self):
        return 'b'

class Queen(Piece):
    
    def __init__(self, color): super().__init__(color)#super(color)
    
    def getMovementType(self):
        return MovementType('diagonal,straight', 8)

    def __str__(self):
        return 'q'

class King(Piece):
    
    def __init__(self, color): super().__init__(color)#super(color)
    
    def getMovementType(self):
        return MovementType('diagonal_straight_kings_not_adjacent,castle', 1)

    #king has to overwrite generic isAttacking because king can move 2 squares when castling but only 1 square distance
    def isAttacking(self, startX, startY, squareX, squareY):

        if startX == squareX and startY == squareY: return False

        reachableInOneMove = self.getMovementType().allowedPositions(startX, startY, self.isWhite())

        reachableInOneMoveAttacks = []

        #filter out forward moves from allowedPositions, leaving only side captures
        for move in reachableInOneMove:
            if startX == move[0]+2:#if 2 squares in x move
                pass
            elif startX == move[0]-2:#if 2 squares in x move
                pass
            else: reachableInOneMoveAttacks.append( (move[0], move[1]) )

        if (squareX, squareY) in reachableInOneMoveAttacks: return True
        return False

    def __str__(self):
        return 'k'