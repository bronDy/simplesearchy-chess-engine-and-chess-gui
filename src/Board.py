from Pieces import Piece
from Pieces import EmptySquare
from MovementType import MovementType

from copy import deepcopy

#all methods work as 1-indexed not 0-indexed (ie getting piece in bottom left corner with getPiece(1, 1))
class Board():
    
    #class fields:
    
    #grid = []
    #may contain only objects derived from Piece or EmptySquare objects
    #access using self.grid[y][x]
    #0-indexed, subtract -1 from x and y given when being called from outside functions
    #standard access: from grid[0][0] to grid[7][7]
    
    #boardXSize = 8
    #boardYSize = 8
    
    def __init__(self, sizeX, sizeY):
        self.grid = []#this makes grid instance member so multiple Board instances can exist each one with its own grid
        
        self.boardXSize, self.boardYSize = sizeX, sizeY
        
        for y in range(sizeY):
            
            self.grid.append([])#add new row
            
            for _ in range(sizeX):
                self.grid[y].append(EmptySquare())
                
    def setPiece(self, x, y, piece):
        x, y = x-1, y-1#make it 0 based because self.grid needs to be indexed using x and y
        
        if not isinstance(piece, Piece):# type(piece) != Piece:
            print('trying to setPiece() in class Board and piece is not of type Piece, but type: ' + str(type(piece)))
            return
        if x < 0 or x >= self.boardXSize or y < 0 or y >= self.boardYSize:
            print('trying to setPiece() in class Board at position x=' + str(x+1) + ', y=' + str(y+1) + ' which is outside of the board')
            return

        self.grid[y][x] = piece

    def setEmptySquare(self, x, y):
        x, y = x-1, y-1#make it 0 based because self.grid needs to be indexed using x and y
        
        if x < 0 or x >= self.boardXSize or y < 0 or y >= self.boardYSize:
            print('trying to setEmptySquare() in class Board at position x=' + str(x+1) + ', y=' + str(y+1) + ' which is outside of the board')
            return
        
        self.grid[y][x] = EmptySquare()

    def swap(self, x1, y1, x2, y2):
        x1, y1 = x1-1, y1-1#make it 0 based because self.grid needs to be indexed using x and y
        x2, y2 = x2-1, y2-1#make it 0 based because self.grid needs to be indexed using x and y
        
        if x1 < 0 or x1 >= self.boardXSize or y1 < 0 or y1 >= self.boardYSize:
            print('trying to swap() in class Board at position x1=' + str(x1+1) + ', y1=' + str(y1+1) + ' which is outside of the board')
            return
        if x2 < 0 or x2 >= self.boardXSize or y2 < 0 or y2 >= self.boardYSize:
            print('trying to swap() in class Board at position x2=' + str(x2+1) + ', y2=' + str(y2+1) + ' which is outside of the board')
            return
            
        self.grid[y1][x1], self.grid[y2][x2] = self.grid[y2][x2], self.grid[y1][x1]

    def getPiece(self, x, y):
        x, y = x-1, y-1#make it 0 based because self.grid needs to be indexed using x and y
        if not (x>=0 and y>=0): print('Trying to getPiece(note: 0-indexed method) from negative index x,y=' + str(x) + ',' + str(y))
        if not (x<=self.boardXSize-1 and y<=self.boardYSize-1): print('Trying to getPiece(note: 0-indexed method) from index greater than board size x,y=' + str(x) + ',' + str(y))
        return self.grid[y][x]

    #returns all Piece instances on the board and their x and y coordinates using a list with tuples
    #return format: [ (Piece(), x1, y1), (Piece(), x2, y2),... ]; x1,y1,x2,y2 are 1-indexed
    def getAllPieces(self):
        lst = []
        for y in range(self.boardYSize):
            for x in range(self.boardXSize):
                if isinstance(self.grid[y][x], Piece): lst.append( (self.grid[y][x], x+1, y+1) )
        return lst

    def rememberPosition(self):
        self.savedPosition = deepcopy(self.grid)
            
    def rollbackPosition(self):
        self.grid = self.savedPosition

    def __str__(self):
        asciiBoard = []
            
        for row in self.grid[::-1]:
            for pieceOrEmptySquare in row:
                if isinstance(pieceOrEmptySquare, EmptySquare): asciiBoard.append((str(pieceOrEmptySquare)))
                elif pieceOrEmptySquare.isWhite(): asciiBoard.append((str(pieceOrEmptySquare)).upper())
                else: asciiBoard.append(str(pieceOrEmptySquare))
            asciiBoard.append('\n')
        return ''.join(asciiBoard)