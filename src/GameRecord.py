class GameRecord():
	#this class remembers game history(castle, en passan square,...) for active game

	#enPassantSquare = [x, y]
	
	#castled[wshort, wlong, bshort, blong]

	#halfMovesConditional = 0

	#fullMoves = 0

	#moves = [ (startSquareX, startSquareY, endSquareX, endSquareY, {promotionPiece}), (startSquareX, startSquareY, endSquareX, endSquareY, {promotionPiece}),... ]
	#{promotionPiece} may or may not exist

	#saved_enPassantSquare, saved_castled, saved_moves

	def __init__(self):

		self.enPassantSquare = [1000, 1000]#dummy x and y values

		whiteCastledShort = False
		whiteCastledLong = False
		blackCastledShort = False
		blackCastledLong = False
		self.castled = [whiteCastledShort, whiteCastledLong, blackCastledShort, blackCastledLong]

		self.halfMovesConditional = 0

		self.fullMoves = 1

		self.moves = []

		self.saved_enPassantSquare, self.saved_castled, self.saved_moves, self.saved_halfMovesConditional, self.saved_fullMoves = None, None, None, None, None
		self.saveGameState()


	def getEnPassantSquareX(self): return self.enPassantSquare[0]
	def getEnPassantSquareY(self): return self.enPassantSquare[1]
	def enPassantSquareExists(self):
		if self.enPassantSquare[0] == 1000: return False
		return True

	def setEnPassantSquareX(self, X): self.enPassantSquare[0] = X
	def setEnPassantSquareY(self, Y): self.enPassantSquare[1] = Y
	def removeEnPassantSquare(self):
		self.enPassantSquare[0] = 1000
		self.enPassantSquare[1] = 1000

	#promotionPiece = 'q', 'r', 'b' or 'n'
	def recordMove(self, startSquareX, startSquareY, endSquareX, endSquareY, promotionPiece=None):
		if promotionPiece == None:
			self.moves.append( (startSquareX, startSquareY, endSquareX, endSquareY) )
		else:
			self.moves.append( (startSquareX, startSquareY, endSquareX, endSquareY, promotionPiece) )

	def recordCastle(self, isSideWhite, rightDirectionBoardRelative):
		if isSideWhite and rightDirectionBoardRelative: self.castled[0] = True
		elif isSideWhite and rightDirectionBoardRelative==False: self.castled[1] = True
		elif isSideWhite==False and rightDirectionBoardRelative: self.castled[2] = True
		elif isSideWhite==False and rightDirectionBoardRelative==False: self.castled[3] = True

	def hasCastled(self, isSideWhite):
		if isSideWhite:
			if self.castled[0] or self.castled[1]: return True
		else:
			if self.castled[2] or self.castled[3]: return True
		return False

	def castledSides(self):
		return self.castled

	def halfmovesSinceLastCaptureOrPawnAdvanceIncrement(self):
		self.halfMovesConditional += 1
	def halfmovesSinceLastCaptureOrPawnAdvanceReset(self):
		self.halfMovesConditional = 0
	def getHalfmovesSinceLastCaptureOrPawnAdvance(self):
		return self.halfMovesConditional

	def fullMovesIncrement(self):
		self.fullMoves += 1
	def getFullMoves(self):
		return self.fullMoves

	def saveGameState(self):
		self.saved_enPassantSquare = self.enPassantSquare[:]
		self.saved_castled = self.castled[:]
		self.saved_moves = self.moves[:]
		self.saved_halfMovesConditional = self.halfMovesConditional
		self.saved_fullMoves = self.fullMoves
	def rollbackGameState(self):
		self.enPassantSquare = self.saved_enPassantSquare[:]
		self.castled = self.saved_castled[:]
		self.moves = self.saved_moves[:]
		self.halfMovesConditional = self.saved_halfMovesConditional
		self.fullMoves = self.saved_fullMoves

	#returns True if piece currently on squareX, squareY was a destination square in one of the previous game moves
	def hasPieceMoved(self, squareX, squareY):

		#look through moves played in a game so far
		for move in self.moves:
			endSquareX, endSquareY = 1000, 1000
			if len(move) == 5: _, _, endSquareX, endSquareY, _ = move#move with promotion
			elif len(move) == 4: _, _, endSquareX, endSquareY = move#move without promotion
			else: print('move:' + str(move) + ' in GameRecord.moves doesn`t have 4 or 5 elements, instead # of elements=' + str(len(move)))

			#if squareX, squareY is recorded as being endSquareX, endSquareY for some move
			if endSquareX == squareX and endSquareY == squareY:
				return True#piece has moved previously
			else:
				pass

		return False