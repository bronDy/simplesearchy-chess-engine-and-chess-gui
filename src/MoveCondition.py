class MoveCondition():

	#self.name

	#x and y if boardRelative==False give directions relative to startPiece which is when creating MoveCondition assumed to be white
	#self.x, self.y

	#boardRelative will allow for x and y to be interpreted as coordinates of the board rather then coordinates of the startPiece
	#boardRelative = False #if True x and y should on normal 8x8 board go from 1 to 8; if x or y is 0 it will not have any effect

	def __init__(self, conditionName, x, y, boardRelative=False):
		self.name = conditionName

		if boardRelative:
			assert x>=0 and y>=0, 'MoveCondition which is boardRelative must be either x and y of `1`-`length of the board` or `0` for that value to be ignored, not x,y='+str(x)+','+str(y)
		self.x, self.y = x, y
		self.boardRelative = boardRelative

	def isBoardRelative(self):
		return self.boardRelative

	def __str__(self):
		return self.name