from Helpers import Helpers
from Pieces import EmptySquare, Pawn
from Board import Board

from GameRecord import GameRecord

#from PiecesSetup import PiecesSetup, StandardPiecesSetup, KingsAndPawnPiecesSetup
from PiecesSetup import *

import sys, os

def blockPrint():
    sys.stdout = open(os.devnull, 'w')

def enablePrint():
    sys.stdout = sys.__stdout__

class BoardManager():

	#class fields:
	alphabetPositions = {'a':1,'b':2,'c':3,'d':4,'e':5,'f':6,'g':7,'h':8,'i':9,'j':10,'k':11,'l':12,'m':13,'n':14,'o':15,'p':16,'q':17,'r':18,'s':19,'t':20,'u':21,'v':22,'w':23,'x':24,'y':25,'z':26}
	#ps = PiecesSetup()
	#board = Board()
	#helpers = Helpers()
	#gameRecord = GameRecord()
	#promotionPiece = 'q'
	#emptySquareChar = EmptySquare()

	def __init__(self, sizeX, sizeY, piecesSetup):
		#self._unitTest_BoardManager()#TURN ON TEST

		assert 1 < sizeX < 27 and 1 < sizeY < 27, 'board size: x=' + str(sizeX) + ', y=' + str(sizeY) + ' not excepted.'

		board = None

		ps = PiecesSetup()#overwrite

		if '/' in piecesSetup:#fen (note: `moves` part of the piecesSetup fen will be ignored)
			board = Board(sizeX, sizeY)
			ps = FENPiecesSetup(piecesSetup)

		elif piecesSetup == 'standard':
			board = Board(sizeX, sizeY)
			ps = StandardPiecesSetup()

		elif piecesSetup == 'KingsAndPawn':
			board = Board(sizeX, sizeY)
			ps = KingsAndPawnPiecesSetup()

		else:
			print('piecesSetup:' + str(piecesSetup) + ' not yet implemented. Creating empty board.')
			board = Board(sizeX, sizeY)

		self.ps = ps
		self.ps.setupPieces(board)

		#set class fields
		self.board = board
		self.helpers = Helpers(self.board)
		self.gameRecord = GameRecord()
		self.promotionPiece = 'q'
		self.emptySquareChar = str(EmptySquare())
	    
	#1-indexed
	#returns True if move was made
	#example call: makeMove('a2', 'a4, True)
	def makeMove(self, startSquare, endSquare, whitesMove):

		try:

			if len(startSquare) != 2 or len(endSquare) not in [2, 3]:
				print('Move not 2 elements long (format examples: `b1`,`c3` , `g7`,`g8q` ,...)')
				return False

			#translate startSquare and endSquare into 4 ints
			startSquareX, startSquareY = self.alphabetPositions[startSquare[0]], int(startSquare[1])
			endSquareX, endSquareY     = self.alphabetPositions[endSquare[0]], int(endSquare[1])

			if len(endSquare) == 3:#promotion
				if endSquare[2] not in ['q','r','b','n']:
					print('Unknown promotion piece:' + endSquare[2] + ', possible promotion pieces:' + str(['q','r','b','n']))
					return False
				self.promotionPiece = endSquare[2]

		except (IndexError, ValueError, KeyError) as error:
			print('Move not parsable (format examples: `b1`, `c3`), error:' + str(error))
			return False

		#if squares within board
		if 1 <= startSquareX <= self.board.boardXSize and 1 <= startSquareY <= self.board.boardYSize and \
		   1 <= endSquareX <= self.board.boardXSize and 1 <= endSquareY <= self.board.boardYSize:

			startPiece = self.board.getPiece(startSquareX, startSquareY)
			
			if isinstance(startPiece, EmptySquare):
				print('Trying to move empty square at position:' + startSquare)
				return False

			if whitesMove != startPiece.isWhite():
				print('Trying to move opponents piece at position:' + startSquare)
				return False

			endPiece = self.board.getPiece(endSquareX, endSquareY)
			if not isinstance(endPiece, EmptySquare):#if not moving to empty square
				if whitesMove == endPiece.isWhite():#if side moving has their piece on move destination square
					print('Trying to capture own piece at position:' + endSquare)
					return False

			self.board.rememberPosition()
			self.gameRecord.saveGameState()
			
			#if piece on startSquare can move to endSquare
			if (endSquareX, endSquareY) in startPiece.getMovementType().allowedPositions(startSquareX, startSquareY, startPiece.isWhite()):

				if startPiece.getMovementType().isJumper() or self.helpers.noPieceInBetween(startSquareX, startSquareY, endSquareX, endSquareY):

					if self.MoveIfConditionsAreFufilled(startPiece, startSquare, endSquare, startSquareX, startSquareY, endSquareX, endSquareY):
						
						#if after whites move white king square is under attack then that white move must be denied
						kingX, kingY = self.helpers.getKingXY(startPiece.isWhite())
						if self.isUnderAttack(kingX, kingY, startPiece.isWhite()):
							self.board.rollbackPosition()
							self.gameRecord.rollbackGameState()
							print('That move would leave own side in check.')
							return False

						return True

				else: print('Piece from', startSquare, 'can`t move to', endSquare, 'because there is a piece in-between.')
			
			else: print('Piece from', startSquare, 'can`t move to', endSquare, 'because its movement rules don`t allow such move.')

		else: print('Piece from', startSquare, 'can`t move to', endSquare, 'because one of the positions is outside of the board.')

		return False

	#returns True if move was made
	def MoveIfConditionsAreFufilled(self, startPiece, startSquare, endSquare, startSquareX, startSquareY, endSquareX, endSquareY):

		#get None or list containing lists of MoveCondition instances
		ListOfmcLists = startPiece.getMovementType().conditions(startSquareX, startSquareY, endSquareX, endSquareY, startPiece.isWhite())

		#if move is unconditional

		if ListOfmcLists == None:
			self.move(startPiece, startSquareX, startSquareY, endSquareX, endSquareY)
			print('Move from', startSquare ,'to' , endSquare)
			return True

		#if there are conditions for move to be made

		for mcLst in ListOfmcLists:

			#if all MoveCondition-s in mcList are satisfied then move can be made
			allConditionsFufilled = True
			for moveCondition in mcLst:

				if self.isConditionSatisfied(moveCondition, startPiece, startSquareX, startSquareY) == False:
					allConditionsFufilled = False

			if allConditionsFufilled:
				self.move(startPiece, startSquareX, startSquareY, endSquareX, endSquareY)
				print('Conditional move from', startSquare ,'to' , endSquare)
				return True#return True if all conditions in any one mcLst are met


		print('Piece from', startSquare, 'can`t move to', endSquare, 'because not all of its movement conditions are fufilled.')
		return False

	def isConditionSatisfied(self, moveCondition, startPiece, startSquareX, startSquareY):

		if not startPiece.isWhite():
			moveCondition.x *= -1
			moveCondition.y *= -1

			if moveCondition.isBoardRelative():
				if moveCondition.x == 0: pass
				else: moveCondition.x = (self.board.boardXSize+1) + moveCondition.x#subtract flipped(negative) moveCondition.x
				if moveCondition.y == 0: pass
				else: moveCondition.y = (self.board.boardYSize+1) + moveCondition.y#subtract flipped(negative) moveCondition.y
				assert ((0 <= moveCondition.x <= self.board.boardXSize) and (0 <= moveCondition.y <= self.board.boardYSize)), 'Board relative conditions x='+str(moveCondition.x)+', y='+str(moveCondition.y)+' are not allowed'


		moveConditionName = str(moveCondition)
		if moveConditionName == 'squareHasEnemyPiece':
			x = startSquareX + moveCondition.x
			y = startSquareY + moveCondition.y
			if x < 1 or x > self.board.boardXSize or y < 1 or y > self.board.boardYSize: return False#x or y outside of board
			if isinstance(self.board.getPiece(x, y), EmptySquare):
				return False#must return immediately because next line would cause error when it would call isWhite() on EmptySquare which is not defined
			if self.board.getPiece(x, y).isWhite() == startPiece.isWhite(): return False
		
		elif moveConditionName == 'enPassantSquare':
			if startSquareX+moveCondition.x != self.gameRecord.getEnPassantSquareX() or \
			   startSquareY+moveCondition.y != self.gameRecord.getEnPassantSquareY(): return False
		
		elif moveConditionName == 'pieceIsOnCertainRank':
			if startSquareY != moveCondition.y: return False

		#kingsNotAdjacent when startPiece moved by moveCondition.x and moveCondition.y
		#would not end up next to another king with exception of king on startSquareX, startSquareY
		elif moveConditionName == 'kingsNotAdjacent':
			endX = startSquareX + moveCondition.x
			endY = startSquareY + moveCondition.y
			#get x,y from all 8 sides
			for x,y in zip([1,0,1,0,1,-1,-1,-1],[1,1,0,-1,-1,0,1,-1]):
				if x == -moveCondition.x and y == -moveCondition.y: pass#ignore startSquare direction
				else:
					if endX+x >= 1 and endY+y >= 1 and endX+x <= self.board.boardXSize and endY+y <= self.board.boardYSize:
						if isinstance(self.board.getPiece(endX+x, endY+y), King): return False

		#squareNotUnderAttack when piece of side opposite from side moving is attacking that square
		#if that square is occupied it is not considered attacked (condition satisfied)
		#if square is outside of the board it is assumed to be under attack
		elif moveConditionName == 'squareNotUnderAttack':
			endX = startSquareX + moveCondition.x
			endY = startSquareY + moveCondition.y
			if endX < 1 or endX > self.board.boardXSize or endY < 1 or endY > self.board.boardYSize: return False#x or y outside of board
			if self.isUnderAttack(endX, endY, startPiece.isWhite()): return False

		elif moveConditionName == 'notPreviouslyCastled':
			if self.gameRecord.hasCastled(startPiece.isWhite()): return False

		elif moveConditionName == 'selfNeverMoved':
			if self.gameRecord.hasPieceMoved(startSquareX, startSquareY): return False

		#closestRookToRightNeverMoved should not be used with board relative = True, moveCondition.x must be != 0
		elif moveConditionName == 'closestRookToRightNeverMoved':
			assert moveCondition.isBoardRelative() == False, 'MoveCondition closestRookToRightNeverMoved should be used with boardRelative = False'
			assert moveCondition.x != 0, 'MoveCondition closestRookToRightNeverMoved must have moveCondition.x=1'
			rookOrNone, rightRookX, rightRookY = None, 1000, 1000
			if moveCondition.x > 0:#white side right is also board relative right
				rookOrNone, rightRookX, rightRookY = self.helpers.getFirstPieceToSide(startSquareX, startSquareY, Rook, True)
			else: rookOrNone, rightRookX, rightRookY = self.helpers.getFirstPieceToSide(startSquareX, startSquareY, Rook, False)
			if rookOrNone == None: return False#no rook to the right
			if self.gameRecord.hasPieceMoved(rightRookX, rightRookY): return False

		#closestRookToLeftNeverMoved should not be used with board relative = True, moveCondition.x must be != 0
		elif moveConditionName == 'closestRookToLeftNeverMoved':
			assert moveCondition.isBoardRelative() == False, 'MoveCondition closestRookToLeftNeverMoved should be used with boardRelative = False'
			assert moveCondition.x != 0, 'MoveCondition closestRookToLeftNeverMoved must have moveCondition.x=1'
			rookOrNone, leftRookX, leftRookY = None, 1000, 1000
			if moveCondition.x > 0:#white side left is also board relative left
				rookOrNone, leftRookX, leftRookY = self.helpers.getFirstPieceToSide(startSquareX, startSquareY, Rook, False)
			else: rookOrNone, leftRookX, leftRookY = self.helpers.getFirstPieceToSide(startSquareX, startSquareY, Rook, True)
			if rookOrNone == None: return False#no rook to the left
			if self.gameRecord.hasPieceMoved(leftRookX, leftRookY): return False

		#opponentNotAttacking2AdjacentRightSquares should not be used with board relative = True, moveCondition.x must be = 1
		#opponentNotAttacking2AdjacentRightSquares does not guarantie that opponent piece is not on one of those 2 squares
		elif moveConditionName == 'opponentNotAttacking2AdjacentRightSquares':
			assert moveCondition.isBoardRelative() == False, 'MoveCondition opponentNotAttacking2AdjacentRightSquares should be used with boardRelative = False'
			x1 = startSquareX + moveCondition.x
			x2 = startSquareX + (moveCondition.x*2)
			y = startSquareY
			if x1 < 1 or x2 < 1 or x1 > self.board.boardXSize or x2 > self.board.boardXSize: return False#x outside of board
			if self.isUnderAttack(x1, y, startPiece.isWhite()) or self.isUnderAttack(x2, y, startPiece.isWhite()): return False
			
		#opponentNotAttacking2AdjacentRightSquares should not be used with board relative = True, moveCondition.x must be = 1
		#opponentNotAttacking2AdjacentRightSquares does not guaranty that opponent piece is not on one of those 2 squares
		elif moveConditionName == 'opponentNotAttacking2AdjacentLeftSquares':
			assert moveCondition.isBoardRelative() == False, 'MoveCondition opponentNotAttacking2AdjacentLeftSquares should be used with boardRelative = False'
			x1 = startSquareX - moveCondition.x
			x2 = startSquareX - (moveCondition.x*2)
			y = startSquareY
			if x1 < 1 or x2 < 1 or x1 > self.board.boardXSize or x2 > self.board.boardXSize: return False#x outside of board
			if self.isUnderAttack(x1, y, startPiece.isWhite()) or self.isUnderAttack(x2, y, startPiece.isWhite()): return False
			
		#example: rule MoveCondition('squareHasNoPiece', -1, 0) applied to white piece on e1 means to check if d1 has no piece;
		#         rule MoveCondition('squareHasNoPiece', -1, 0) applied to black piece on e1 means to check if f1 has no piece
		#squareHasNoPiece should not be used with board relative = True (ie. MoveCondition('squareHasNoPiece', x, y, True))
		elif moveConditionName == 'squareHasNoPiece':
			assert moveCondition.isBoardRelative() == False, 'MoveCondition squareHasNoPiece should be used with boardRelative = False'
			x = startSquareX + moveCondition.x
			y = startSquareY + moveCondition.y
			if x < 1 or x > self.board.boardXSize or y < 1 or y > self.board.boardYSize: return False#x or y outside of board
			if not isinstance(self.board.getPiece(x, y), EmptySquare): return False
			
		elif moveConditionName == 'pieceIsWhite':
			if not startPiece.isWhite(): return False

		elif moveConditionName == 'pieceIsBlack':
			if startPiece.isWhite(): return False
			
		else:
			print('Condition:' + str(moveCondition) + ' not recognized by BoardManager.isConditionSatisfied(). Not allowing move to be made.')
			return False

		return True

	#returns False if checkmatedSideIsWhite is not in checkmate nor in stalemate
	#returns True if there are no legal moves for checkmatedSideIsWhite=True or False (white or black in checkmate)
	#returns 'stalemate' if checkmatedSideIsWhite has no moves and it's king is not under attack
	#this method will enablePrint() before returning
	def inCheckmate(self, checkmatedSideIsWhite):
		blockPrint()

		for piece, pieceX, pieceY in self.board.getAllPieces():#all pieces on the board
			if piece.isWhite() != checkmatedSideIsWhite: continue#ignore pieces of enemy side

			for (xDest, yDest) in piece.getMovementType().allowedPositions(pieceX, pieceY, checkmatedSideIsWhite):#possible moves
				if xDest < 1 or yDest < 1: continue#skip if x or y outside of the board in negative direction
				if xDest > self.board.boardXSize or yDest > self.board.boardXSize: continue#skip if outside of the board in positive direction

				#translate 4 ints into startSquare and endSquare strings
				xStartSquare = ''
				xEndSquare = ''
				for k, v in self.alphabetPositions.items():
					if v == pieceX:
						xStartSquare = k
					if v == xDest:
						xEndSquare = k

				if self.makeMove(xStartSquare+str(pieceY), xEndSquare+str(yDest), checkmatedSideIsWhite):

					#don't actually make the move (in case move is not possible roll back would happen automatically in self.makeMove())
					self.board.rollbackPosition()
					self.gameRecord.rollbackGameState()

					enablePrint()
					return False#if legal move can be made then there is no checkmate

		#no legal moves found for any piece on board whose .isWhite() == checkmatedSideIsWhite
		#at this point it is either checkmate or stalemate

		kX, kY = self.helpers.getKingXY(checkmatedSideIsWhite)
		enablePrint()
		if self.isUnderAttack(kX, kY, checkmatedSideIsWhite):#king is under attack
			return True
		else: return 'stalemate'#king not under attack

	#isUnderAttack returns True if squareX, squareY is under attack by any piece on the enemy side
	#sideBeingAttackedIsWhite = True or False, if square is occupied it is not under attack
	#1-indexed
	def isUnderAttack(self, squareX, squareY, sideBeingAttackedIsWhite):

		#for every piece(pieceX, pieceY) on the board
		for piece, pieceX, pieceY in self.board.getAllPieces():#all pieces on the board
			if piece.isWhite() != sideBeingAttackedIsWhite:#if piece is enemy piece
				#if this piece is a jumper or path is not blocked by another piece
				if piece.getMovementType().isJumper() or self.helpers.noPieceInBetween(pieceX, pieceY, squareX, squareY):
					if piece.isAttacking(pieceX, pieceY, squareX, squareY):#if piece is attacking squareX, squareY
						return True

		return False

	def move(self, startPiece, startSquareX, startSquareY, endSquareX, endSquareY):

		#remember enPassantSquare
		newEnPassantSquare = False
		if isinstance(startPiece, Pawn):
			if startPiece.isWhite():
				if startSquareY+2 == endSquareY:
					self.gameRecord.setEnPassantSquareX(endSquareX)
					self.gameRecord.setEnPassantSquareY(endSquareY-1)
					newEnPassantSquare = True
			else:
				if startSquareY-2 == endSquareY:
					self.gameRecord.setEnPassantSquareX(endSquareX)
					self.gameRecord.setEnPassantSquareY(endSquareY+1)
					newEnPassantSquare = True

		#remember side castled
		if isinstance(startPiece, King):
			if startSquareX + 2 == endSquareX:
				self.gameRecord.recordCastle(startPiece.isWhite(), True)
			if startSquareX - 2 == endSquareX:
				self.gameRecord.recordCastle(startPiece.isWhite(), False)

		#remember numer of half moves since promotion or capture
		conditionalHalfMove = False#False = condition(pawn move, capture, promotion) to reset halfmove counter not met

		#remember numer of half moves since promotion or capture
		if not startPiece.isWhite():
			self.gameRecord.fullMovesIncrement()


		#special moves

		#is this castling move? Bring rook next to the king
		if isinstance(startPiece, King):
			if startSquareX + 2 == endSquareX:
				
				#get rook to the side
				castleRook, rookX, rookY = self.helpers.getFirstPieceToSide(startSquareX, startSquareY, Rook, True)
				
				#swap rook with square adjacent to the king
				self.board.swap(startSquareX + 1, startSquareY, rookX, rookY)

			if startSquareX - 2 == endSquareX:
				
				#get rook to the side
				castleRook, rookX, rookY = self.helpers.getFirstPieceToSide(startSquareX, startSquareY, Rook, False)
				
				#swap rook with square adjacent to the king
				self.board.swap(startSquareX - 1, startSquareY, rookX, rookY)


		#is this en passant move? Remove pawn to the side
		if isinstance(startPiece, Pawn):
			conditionalHalfMove = True#pawn move or en passant
			if self.gameRecord.getEnPassantSquareX() == endSquareX and self.gameRecord.getEnPassantSquareY() == endSquareY:
				#remove enemy pawn to the side
				if startSquareX - 1 == endSquareX:
					self.board.setEmptySquare(startSquareX - 1, startSquareY)
				else:
					self.board.setEmptySquare(startSquareX + 1, startSquareY)


		#normal move or last step in special move

		#if this move doesn't allow next opponent action to be en passant move
		if newEnPassantSquare == False: self.gameRecord.removeEnPassantSquare()

		endPiece = self.board.getPiece(endSquareX, endSquareY)
		if isinstance(endPiece, EmptySquare):
			#move piece by swapping destination EmptySquare instance with startPiece
			#startPiece, endPiece = endPiece, startPiece
			self.board.swap(startSquareX, startSquareY, endSquareX, endSquareY)

		#if endPiece is not EmptySquare it should be removed from the board (capture)
		else:
			#move piece by replacing destination piece with startPiece;
			#and creating new EmptySquare instance and putting it on previous startPiece square
			#startPiece, endPiece = EmptySquare(), startPiece
			self.board.setPiece(endSquareX, endSquareY, startPiece)
			self.board.setEmptySquare(startSquareX, startSquareY)

			conditionalHalfMove = True#capture

		
		moveRecorded = False
		#pawn promotion
		ep = self.board.getPiece(endSquareX, endSquareY)
		if isinstance(ep, Pawn):
			if ep.isWhite():
				if endSquareY == self.board.boardYSize:
					self.board.setPiece(endSquareX, endSquareY, self.ps.newPiece(self.promotionPiece, ep.isWhite()))
			else:
				if endSquareY == 1:
					self.board.setPiece(endSquareX, endSquareY, self.ps.newPiece(self.promotionPiece, ep.isWhite()))

			#if endSquareX, endSquareY no longer has Pawn (then it promoted)
			if not isinstance(self.board.getPiece(endSquareX, endSquareY), Pawn):
				#remember promotion move
				self.gameRecord.recordMove(startSquareX, startSquareY, endSquareX, endSquareY, self.promotionPiece)
				moveRecorded = True
				conditionalHalfMove = True#pawn promotion

		self.promotionPiece = 'q'#reset defult
		if not moveRecorded:
			self.gameRecord.recordMove(startSquareX, startSquareY, endSquareX, endSquareY)#remember move

		if conditionalHalfMove: self.gameRecord.halfmovesSinceLastCaptureOrPawnAdvanceReset()
		else: self.gameRecord.halfmovesSinceLastCaptureOrPawnAdvanceIncrement()
		
	def getGameRecord(self):
		return self.gameRecord

	def __str__(self):
		return str(self.board)

	def displayBoard(self):
		#print(str(self.board))#simple board display
		#complex board display
		print('  +' + '---+'*(len(str(self.board).split('\n'))-1))
		rows = filter(lambda x:x!='', str(self.board).split('\n'))#rows = str(self.board).split('\n')[:-1:]
		i = self.board.boardYSize
		for row in rows:
			print(str(i)+' | ', end='')
			i -= 1
			for char in row.replace(self.emptySquareChar, ' '):
				print(char+' | ', end='')
			print()
			print('  ' + '+---'*len(row), end='')
			print('+')
		print('    a   b   c   d   e   f   g   h\n')
    
	#this test should be run before using this class because multiple class fields will be overwritten by this test
	#during this test inCheckmate(), makeMove() and all cascading functions will be called from it
	def _unitTest_BoardManager(self):
		print('running', '_unitTest_BoardManager')

		self.gameRecord = GameRecord()
		self.promotionPiece = 'q'
		#checkmate position
		self.board = Board(8, 8)
		self.helpers = Helpers(self.board)
		whiteKing = King('w')
		self.board.setPiece(8, 6, whiteKing)#h6
		blackKing = King('b')
		self.board.setPiece(8, 8, blackKing)#h8
		whiteRook = Rook('w')
		self.board.setPiece(2, 8, whiteRook)#b8

		assert self.inCheckmate(True) == False, 'failed unit test n1: inCheckmate(True) = False' + ', got:' + str(self.inCheckmate(True))
		assert self.inCheckmate(False), 'failed unit test n2: inCheckmate(False) = True' + ', got:' + str(self.inCheckmate(False))

		#stalemate position
		self.board = Board(8, 8)
		self.helpers = Helpers(self.board)
		whiteKing = King('w')
		self.board.setPiece(4, 6, whiteKing)#d6
		blackKing = King('b')
		self.board.setPiece(4, 8, blackKing)#d8
		whitePawn = Pawn('w')
		self.board.setPiece(4, 7, whitePawn)#d7

		assert self.inCheckmate(True) == False, 'failed unit test n3: inCheckmate(True) = False' + ', got:' + str(self.inCheckmate(True))
		assert self.inCheckmate(False) == 'stalemate', 'failed unit test n4: inCheckmate(False) = stalemate' + ', got:' + str(self.inCheckmate(False))

		#position with only kings
		self.board = Board(8, 8)
		self.helpers = Helpers(self.board)
		whiteKing = King('w')
		self.board.setPiece(2, 3, whiteKing)#b3
		blackKing = King('b')
		self.board.setPiece(1, 1, blackKing)#a1

		assert not self.inCheckmate(True), 'failed unit test n5: inCheckmate(True) = False' + ', got:' + str(self.inCheckmate(True))
		assert not self.inCheckmate(False), 'failed unit test n6: inCheckmate(False) = False' + ', got:' + str(self.inCheckmate(False))

		del self.helpers
		del self.gameRecord
		del self.promotionPiece
		del self.board
		print('finished', '_unitTest_BoardManager\n')