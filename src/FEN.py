from Pieces import *

class FEN():

	def getStandardPiecesSetup(self):

		startingFEN = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
		#startingFEN = 'r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R w KQkq - 0 1'#castle test
		return startingFEN


	#this method does not work with fen strings which have `moves` part
	#(ex. string='6q1/5k2/8/8/8/8/PPP1K3/8 w - - 0 1 moves a2a3')
	#only part before first space ('6q1/5k2/8/8/8/8/PPP1K3/8') will be applied to the board
	def applyFenPositionStringToBoard(self, string, board):

		x = 1
		y = board.boardYSize
		for row in string.split(' ')[0].split('/'):

			for char in row:

				if not char.isalpha():
					x += int(char)
					continue
				
				color = 'white'
				if char.islower(): color = 'black'

				char = char.lower()

				if char == 'p': board.setPiece(x, y, Pawn(color))
				elif char == 'r': board.setPiece(x, y, Rook(color))
				elif char == 'n': board.setPiece(x, y, Knight(color))
				elif char == 'b': board.setPiece(x, y, Bishop(color))
				elif char == 'q': board.setPiece(x, y, Queen(color))
				elif char == 'k': board.setPiece(x, y, King(color))
				else: print('FEN character not a known piece', char)

				x += 1

			y -= 1
			x = 1
			

	def getFENFromBoard(self, board, gameRecord, isWhite):

		FEN = []
		emptySquare = EmptySquare()

		emptySquaresInRow = 0
		for row in board.split('\n')[:-1:]:

			for pieceString in row:

				if pieceString == str(emptySquare):
					emptySquaresInRow += 1
					continue
				if emptySquaresInRow != 0:
					FEN.append(str(emptySquaresInRow))
					emptySquaresInRow = 0

				FEN.append(pieceString)

			#last element in row was emptySquare
			if emptySquaresInRow != 0:#whole row of emptySquare elements
				FEN.append(str(emptySquaresInRow))
				emptySquaresInRow = 0

			FEN.append('/')

		finalFEN = ''.join(FEN[:-1:])#remove last '/'


		if isWhite: finalFEN += ' w '
		else: finalFEN += ' b '


		castledState = ''
		whiteCastledShort, whiteCastledLong, blackCastledShort, blackCastledLong = gameRecord.castledSides()
		if not whiteCastledShort: castledState += 'K'
		if not whiteCastledLong: castledState += 'Q'
		if not blackCastledShort: castledState += 'k'
		if not blackCastledLong: castledState += 'q'

		if castledState != '': finalFEN += castledState
		else: finalFEN += '-'
		finalFEN += ' '


		enPassantTargetSquare = ''
		if gameRecord.enPassantSquareExists():
			enPassantTargetSquare += str(gameRecord.getEnPassantSquareX())+str(gameRecord.getEnPassantSquareY())
		else: enPassantTargetSquare = '-'

		finalFEN += enPassantTargetSquare + ' '


		finalFEN += str(gameRecord.getHalfmovesSinceLastCaptureOrPawnAdvance()) + ' '


		finalFEN += str(gameRecord.getFullMoves())

		return finalFEN