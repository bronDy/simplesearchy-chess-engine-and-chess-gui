from Pieces import *
from FEN import FEN

class PiecesSetup():

	def setupPieces(self, board): print('Warrning: setupPieces(self, board) not overwritten')

	def newPiece(self, pieceChar, isWhite):

		assert type(isWhite) is bool, 'PiecesSetup.newPiece() not given boolean isWhite, instead type of isWhite:' + str(type(isWhite))
		
		color = 'newPieceColor'
		if isWhite: color = 'white'
		else: color = 'black'

		if pieceChar == 'q': return Queen(color)
		elif pieceChar == 'r': return Rook(color)
		elif pieceChar == 'b': return Bishop(color)
		elif pieceChar == 'n': return Knight(color)
		else:
			print('Can`t create piece with char:' + pieceChar + ', of color:' + color + '. Making ' + color + ' queen.')
			return Queen(color)


class StandardPiecesSetup(PiecesSetup):

	def setupPieces(self, board):

		fen = FEN()

		fenPosition = fen.getStandardPiecesSetup()

		fen.applyFenPositionStringToBoard(fenPosition, board)


class FENPiecesSetup(PiecesSetup):

	def __init__(self, fenString):

		self.fenString = fenString

	def setupPieces(self, board):

		fen = FEN()

		fen.applyFenPositionStringToBoard(self.fenString, board)


class KingsAndPawnPiecesSetup(PiecesSetup):

	def setupPieces(self, board):

		king = King('w')
		board.setPiece(5, 1, king)

		pawn = Pawn('w')
		board.setPiece(5, 2, pawn)

		kingBlack = King('b')
		board.setPiece(5, 8, kingBlack)