from copy import deepcopy
from Pieces import *

class MinimaxWithAlphaBeta():

	def __init__(self, boardMenager):
		self.bm = deepcopy(boardMenager)

		#mate in 2, 7 pieces, minimum recursionDepth = 4(1 sec)
		#kbK5/pp6/1P6/8/8/8/8/R7 w - -

		#starting position without pawns, used for speed testing
		#rnbqkbnr/8/8/8/8/8/8/RNBQKBNR w KQq - 0 1

		#recursionDepth = 3 needed to see queen winning tactic, 20 pieces(~5 sec)
		#7r/2p2pk1/1p1p2p1/1P1Pp1P1/p1P1P2q/P5R1/3Q2K1/6R1 b - - 0 10

		#mate in 3, 14 pieces
		#Q7/5p2/5P1p/5PPN/6Pk/4N1Rp/7P/6K1 w KQkq - 0 1

		#mate in 1:6k1/8/7K/8/8/8/8/R7 w - - 0 1

	def move(self, sideToMoveIsWhite):

		numberOfPiecesOnBoard = self.countPieces(self.bm, True)+self.countPieces(self.bm, False)

		if numberOfPiecesOnBoard >= 6:
			recursionDepth = 2
			return self.minimaxWithAB(self.bm, sideToMoveIsWhite, recursionDepth, -1000, 1000)[1]

		#solving any mate in 2(4 halfmoves) with up to 5 pieces on the board
		elif numberOfPiecesOnBoard <= 5 and numberOfPiecesOnBoard >= 3:
			recursionDepth = 4
			return self.minimaxWithAB(self.bm, sideToMoveIsWhite, recursionDepth, -1000, 1000)[1]

		else: return 'draw'#game is a draw with less than 2 pieces


	#position 'goodness' from sideToMoveIsWhite perspective
	def heuristic(self, bm, sideToMoveIsWhite):

		#how many pieces does sideToMoveIsWhite have - how many pieces does the opponent have
		#return self.valueOfPieces(bm, sideToMoveIsWhite) - self.valueOfPieces(bm, not sideToMoveIsWhite)
		return self.sideValue(bm, sideToMoveIsWhite) - self.sideValue(bm, not sideToMoveIsWhite)

	def minimaxWithAB(self, bm, maximizingPlayer, recursionDepth, a, b):


		if recursionDepth == 0: return (self.heuristic(bm, True), '')


		if maximizingPlayer:

			moveValue = -1000
			bestMove = ''
			sideToMoveIsWhite = True


			#for every move
			for piece, pieceX, pieceY in bm.board.getAllPieces():

				#if piece not of side sideToMoveIsWhite then skip it
				if piece.isWhite() != sideToMoveIsWhite: continue
				#if isinstance(piece, King): continue#don't make king moves

				for (xDest, yDest) in piece.getMovementType().allowedPositions(pieceX, pieceY, sideToMoveIsWhite):

					#translate 4 ints into startSquare and endSquare strings
					xStartSquare = ''
					xEndSquare = ''
					for k, v in bm.alphabetPositions.items():
						if v == pieceX:
							xStartSquare = k
						if v == xDest:
							xEndSquare = k

					#if legal move
					if bm.makeMove(xStartSquare+str(pieceY), xEndSquare+str(yDest), sideToMoveIsWhite):

						recursiveMoveValue = self.minimaxWithAB(deepcopy(bm), False, recursionDepth-1, a, b)[0]
						if moveValue <= recursiveMoveValue:
							bestMove = str(xStartSquare+str(pieceY) + xEndSquare+str(yDest))

						moveValue = max(moveValue, recursiveMoveValue)
						a = max(a, moveValue)

						#don't actually make the move (in case move is not possible rollback would happen automaticlly in bm.makeMove())
						bm.board.rollbackPosition()
						bm.gameRecord.rollbackGameState()

						if a > b: return (a, str(xStartSquare+str(pieceY) + xEndSquare+str(yDest)))


		else:

			moveValue = 1000
			bestMove = ''
			sideToMoveIsWhite = False


			#for every move
			for piece, pieceX, pieceY in bm.board.getAllPieces():

				#if piece not of side sideToMoveIsWhite then skip it
				if piece.isWhite() != sideToMoveIsWhite: continue
				#if isinstance(piece, King): continue#don't make king moves

				for (xDest, yDest) in piece.getMovementType().allowedPositions(pieceX, pieceY, sideToMoveIsWhite):

					#translate 4 ints into startSquare and endSquare strings
					xStartSquare = ''
					xEndSquare = ''
					for k, v in bm.alphabetPositions.items():
						if v == pieceX:
							xStartSquare = k
						if v == xDest:
							xEndSquare = k

					#if legal move
					if bm.makeMove(xStartSquare+str(pieceY), xEndSquare+str(yDest), sideToMoveIsWhite):

						recursiveMoveValue = self.minimaxWithAB(deepcopy(bm), True, recursionDepth-1, a, b)[0]
						if recursiveMoveValue <= moveValue:
							bestMove = str(xStartSquare+str(pieceY) + xEndSquare+str(yDest))

						moveValue = min(moveValue, recursiveMoveValue)
						b = min(b, moveValue)

						#don't actually make the move (in case move is not possible rollback would happen automaticlly in bm.makeMove())
						bm.board.rollbackPosition()
						bm.gameRecord.rollbackGameState()

						if b < a: return (b, str(xStartSquare+str(pieceY) + xEndSquare+str(yDest)))


		return (moveValue, bestMove)


	#count number of sideIsWhite pieces on the board
	def countPieces(self, bm, sideIsWhite):

		numberOfPiecesOnBoard = 0

		#for every piece in resulting position
		for p, pX, pY in bm.board.getAllPieces():

			#if piece is sideIsWhite
			if p.isWhite() == sideIsWhite:

				numberOfPiecesOnBoard += 1

		return numberOfPiecesOnBoard


	#count value of sideIsWhite pieces on the board
	def valueOfPieces(self, bm, sideIsWhite):

		numberOfPiecesOnBoard = 0

		#for every piece in resulting position
		for p, pX, pY in bm.board.getAllPieces():

			#if piece is sideIsWhite
			if p.isWhite() == sideIsWhite:

				if isinstance(p, Pawn):
					numberOfPiecesOnBoard += 1
				elif isinstance(p, Knight):
					numberOfPiecesOnBoard += 3
				elif isinstance(p, Bishop):
					numberOfPiecesOnBoard += 3
				elif isinstance(p, Rook):
					numberOfPiecesOnBoard += 5
				elif isinstance(p, Queen):
					numberOfPiecesOnBoard += 9
				elif isinstance(p, King):
					numberOfPiecesOnBoard += 50

		return numberOfPiecesOnBoard


	#advanced version of valueOfPieces
	def sideValue(self, bm, sideIsWhite):

		value = 0#value of the position from perspective of sideIsWhite=True or False

		#for every piece in resulting position
		for p, pX, pY in bm.board.getAllPieces():

			#if piece is sideIsWhite
			if p.isWhite() == sideIsWhite:
				#avoid having pieces on first 2 and last 2 ranks (move foreword)
				if pY == 1 or pY == 7 or pY == 2 or pY == 6: value -= 1

				if isinstance(p, Pawn):
					if pX > 2 and pX < 7: value += 11#central pawns are more valuable
					else: value += 10
				elif isinstance(p, Knight):
					if pX == 1 or pX == 8: value += 30#avoid edge of the board
					else: value += 31
				elif isinstance(p, Bishop):
					value += 30
				elif isinstance(p, Rook):
					if pX == 4 or pX == 5: value += 52#put rooks in the center
					elif pX == 1 or pX == 8: value += 51#don't move rooks from original squares in the opening
					else: value += 50
				elif isinstance(p, Queen):
					value += 120
				elif isinstance(p, King):
					if sideIsWhite == True:#white
						if pY == 1: value += 505#stay on first rank
						else: value += 500
					else:#black
						if pY == 8: value += 505#stay on last rank
						else: value += 500
		
		#positional state
		if sideIsWhite == True:#white
			#central control
			if bm.isUnderAttack(4, 5, False): value += 4
			if bm.isUnderAttack(5, 5, False): value += 4
			if bm.isUnderAttack(4, 4, False): value += 2
			if bm.isUnderAttack(5, 4, False): value += 2
			
		else:#black
			#central control
			if bm.isUnderAttack(4, 4, True): value += 4
			if bm.isUnderAttack(5, 4, True): value += 4
			if bm.isUnderAttack(4, 5, True): value += 2
			if bm.isUnderAttack(5, 5, True): value += 2
			

		return value