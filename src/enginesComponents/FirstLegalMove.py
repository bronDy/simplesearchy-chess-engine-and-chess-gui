class FirstLegalMove():

	def __init__(self, boardMenager):
		self.bm = boardMenager

	def move(self, sideToMoveIsWhite):

		for piece, pieceX, pieceY in self.bm.board.getAllPieces():

			#if piece not of side sideToMoveIsWhite then skip it
			if piece.isWhite() != sideToMoveIsWhite: continue

			for (xDest, yDest) in piece.getMovementType().allowedPositions(pieceX, pieceY, sideToMoveIsWhite):

				#translate 4 ints into startSquare and endSquare strings
				xStartSquare = ''
				xEndSquare = ''
				for k, v in self.bm.alphabetPositions.items():
					if v == pieceX:
						xStartSquare = k
					if v == xDest:
						xEndSquare = k

				if self.bm.makeMove(xStartSquare+str(pieceY), xEndSquare+str(yDest), sideToMoveIsWhite):

					#don't actually make the move (in case move is not possible rollback would happen automaticlly in bm.makeMove())
					self.bm.board.rollbackPosition()
					self.bm.gameRecord.rollbackGameState()

					return str(xStartSquare+str(pieceY) + xEndSquare+str(yDest))

		return 'a8d7'#only reached if there are no possible moves