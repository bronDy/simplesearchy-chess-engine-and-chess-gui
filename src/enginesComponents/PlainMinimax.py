from copy import deepcopy
from Pieces import *

class PlainMinimax():

	def __init__(self, boardMenager):
		self.bm = deepcopy(boardMenager)

		#mate in 2, 7 pieces, minimum recursionDepth = 4(~1 min)
		#kbK5/pp6/1P6/8/8/8/8/R7 w - -

		#starting position without pawns, used for speed testing
		#rnbqkbnr/8/8/8/8/8/8/RNBQKBNR w KQq - 0 1

		#recursionDepth = 3 needed to see queen winning tactic(~20 sec)
		#7r/2p2pk1/1p1p2p1/1P1Pp1P1/p1P1P2q/P5R1/3Q2K1/6R1 b - - 0 10

	def move(self, sideToMoveIsWhite):

		numberOfPiecesOnBoard = self.countPieces(self.bm, True)+self.countPieces(self.bm, False)

		if numberOfPiecesOnBoard >= 6:
			recursionDepth = 2
			return self.minimax(self.bm, sideToMoveIsWhite, recursionDepth)[1]
		
		#solving any mate in 2(4 halfmoves) with up to 5 pieces on the board
		elif numberOfPiecesOnBoard <= 5 and numberOfPiecesOnBoard >= 3:
			recursionDepth = 4
			return self.minimax(self.bm, sideToMoveIsWhite, recursionDepth)[1]

		else: return 'draw'#game is a draw with less than 2 pieces


	#position 'goodness' from sideToMoveIsWhite perspective
	def heuristic(self, bm, sideToMoveIsWhite):

		#how many pieces does sideToMoveIsWhite have - how many pieces does the opponent have
		return self.countPiecesValue(bm, sideToMoveIsWhite) - self.countPiecesValue(bm, not sideToMoveIsWhite)

	def minimax(self, bm, maximizingPlayer, recursionDepth):


		if recursionDepth == 0: return (self.heuristic(bm, True), '')


		if maximizingPlayer:

			moveValue = -1000
			bestMove = ''
			sideToMoveIsWhite = True


			#for every move
			for piece, pieceX, pieceY in bm.board.getAllPieces():

				#if piece not of side sideToMoveIsWhite then skip it
				if piece.isWhite() != sideToMoveIsWhite: continue

				for (xDest, yDest) in piece.getMovementType().allowedPositions(pieceX, pieceY, sideToMoveIsWhite):

					#translate 4 ints into startSquare and endSquare strings
					xStartSquare = ''
					xEndSquare = ''
					for k, v in bm.alphabetPositions.items():
						if v == pieceX:
							xStartSquare = k
						if v == xDest:
							xEndSquare = k

					#if legal move
					if bm.makeMove(xStartSquare+str(pieceY), xEndSquare+str(yDest), sideToMoveIsWhite):

						recursiveMoveValue = self.minimax(deepcopy(bm), False, recursionDepth-1)[0]
						if moveValue <= recursiveMoveValue:
							bestMove = str(xStartSquare+str(pieceY) + xEndSquare+str(yDest))

						moveValue = max(moveValue, recursiveMoveValue)

						#don't actually make the move (in case move is not possible rollback would happen automaticlly in bm.makeMove())
						bm.board.rollbackPosition()
						bm.gameRecord.rollbackGameState()


		else:

			moveValue = 1000
			bestMove = ''
			sideToMoveIsWhite = False


			#for every move
			for piece, pieceX, pieceY in bm.board.getAllPieces():

				#if piece not of side sideToMoveIsWhite then skip it
				if piece.isWhite() != sideToMoveIsWhite: continue

				for (xDest, yDest) in piece.getMovementType().allowedPositions(pieceX, pieceY, sideToMoveIsWhite):

					#translate 4 ints into startSquare and endSquare strings
					xStartSquare = ''
					xEndSquare = ''
					for k, v in bm.alphabetPositions.items():
						if v == pieceX:
							xStartSquare = k
						if v == xDest:
							xEndSquare = k

					#if legal move
					if bm.makeMove(xStartSquare+str(pieceY), xEndSquare+str(yDest), sideToMoveIsWhite):

						recursiveMoveValue = self.minimax(deepcopy(bm), True, recursionDepth-1)[0]
						if recursiveMoveValue <= moveValue:
							bestMove = str(xStartSquare+str(pieceY) + xEndSquare+str(yDest))

						moveValue = min(moveValue, recursiveMoveValue)

						#don't actually make the move (in case move is not possible rollback would happen automaticlly in bm.makeMove())
						bm.board.rollbackPosition()
						bm.gameRecord.rollbackGameState()


		return (moveValue, bestMove)
		

	#count number of sideIsWhite pieces on the board
	def countPieces(self, bm, sideIsWhite):

		numberOfPiecesOnBoard = 0

		#for every piece in resulting position
		for p, pX, pY in bm.board.getAllPieces():

			#if piece is sideIsWhite
			if p.isWhite() == sideIsWhite:

				numberOfPiecesOnBoard += 1

		return numberOfPiecesOnBoard

	#count value of sideIsWhite pieces on the board
	def countPiecesValue(self, bm, sideIsWhite):

		numberOfPiecesOnBoard = 0

		#for every piece in resulting position
		for p, pX, pY in bm.board.getAllPieces():

			#if piece is sideIsWhite
			if p.isWhite() == sideIsWhite:

				if isinstance(p, Pawn):
					numberOfPiecesOnBoard += 1
				elif isinstance(p, Knight):
					numberOfPiecesOnBoard += 3
				elif isinstance(p, Bishop):
					numberOfPiecesOnBoard += 3
				elif isinstance(p, Rook):
					numberOfPiecesOnBoard += 5
				elif isinstance(p, Queen):
					numberOfPiecesOnBoard += 9
				elif isinstance(p, King):
					numberOfPiecesOnBoard += 100

		return numberOfPiecesOnBoard