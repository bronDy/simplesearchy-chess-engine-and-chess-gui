from MoveCondition import MoveCondition

class MovementType():

	#allowedCoordinates format: [ ( x1, y1, (MoveCondition('cond1'), MoveCondition('cond2')) ), ( x2, y2, (MoveCondition('cond1'), MoveCondition('cond4')) ) ]
	#allowedCoordinates's MoveCondition list may be instead left as None
	#allowedCoordinates = []

	#jumper = False

	def __init__(self, moveType, distance, jumper = False):
		#moveType is comma separated list of movementType-s (ex. for queen moveType='diagonal,straight')
		#distance is only used for certain moveType-s (diagonal, vertical,...), for others (knight_move, diagonal_capture,...) it is ignored
		#if jumper is set to True there will be no check to see if there are pieces in-between start and end square on any move

		assert type(moveType) == str, 'moveType must be of type `str`, not of type:' + str(type(moveType))
		assert type(distance) == int, 'distance must be of type `int`, not of type:' + str(type(distance))
		assert type(jumper) == bool, 'jumper must be of type `bool`, not of type:' + str(type(jumper))

		self.allowedCoordinates = []
		self.jumper = False

		for movementType in moveType.split(','):

			#x, y are flipped in case of black perspective;
			#MoveCondition.x, MoveCondition.y are not flipped if MoveCondition.boardRelative=True
			
			if movementType == 'diagonal':
				x, y = 0, 0
				for d in range(1, distance+1):
					self.allowedCoordinates.append((x+d, y+d, None))
					self.allowedCoordinates.append((x+d, y-d, None))
					self.allowedCoordinates.append((x-d, y+d, None))
					self.allowedCoordinates.append((x-d, y-d, None))

			elif movementType == 'straight':
				x, y = 0, 0
				for d in range(1, distance+1):
					self.allowedCoordinates.append((x+d, y, None))
					self.allowedCoordinates.append((x-d, y, None))
					self.allowedCoordinates.append((x, y+d, None))
					self.allowedCoordinates.append((x, y-d, None))

			elif movementType == 'vertical_foreward':
				x, y = 0, 0
				for d in range(1, distance+1):
					self.allowedCoordinates.append((x, y+d, None))

			elif movementType == 'horizontal_right':
				x, y = 0, 0
				for d in range(1, distance+1):
					self.allowedCoordinates.append((x+d, y, None))

			elif movementType == 'vertical':
				x, y = 0, 0
				for d in range(1, distance+1):
					self.allowedCoordinates.append((x, y+d, None))
					self.allowedCoordinates.append((x, y-d, None))

			elif movementType == 'horizontal':
				x, y = 0, 0
				for d in range(1, distance+1):
					self.allowedCoordinates.append((x+d, y, None))
					self.allowedCoordinates.append((x-d, y, None))

			elif movementType == 'double_move_from_second_rank':
				x, y = 0, 0
				self.allowedCoordinates.append( ( x, y+2, ( MoveCondition('pieceIsOnCertainRank', 0, 2, True), MoveCondition('squareHasNoPiece', 0, 1), MoveCondition('squareHasNoPiece', 0, 2) ) ) )

			elif movementType == 'diagonal_capture':
				x, y = 0, 0
				for d in range(1, distance+1):
					self.allowedCoordinates.append( ( x+d, y+d, ( MoveCondition('squareHasEnemyPiece', +1, +1), ) ) )
					self.allowedCoordinates.append( ( x-d, y+d, ( MoveCondition('squareHasEnemyPiece', -1, +1), ) ) )

			elif movementType == 'vertical_one_square_foreward_on_empty_square':
				self.allowedCoordinates.append( ( 0, 1, ( MoveCondition('squareHasNoPiece', 0, 1), ) ) )

			elif movementType == 'en_passant':
				x, y = 0, 0
				for d in range(1, distance+1):
					self.allowedCoordinates.append( ( x+d, y+d, ( MoveCondition('pieceIsOnCertainRank', 0, 5, True), MoveCondition('enPassantSquare', +1, +1) ) ) )
					self.allowedCoordinates.append( ( x-d, y+d, ( MoveCondition('pieceIsOnCertainRank', 0, 5, True), MoveCondition('enPassantSquare', -1, +1) ) ) )

			elif movementType == 'castle':
				x, y = 0, 0
				self.allowedCoordinates.append( ( x+2, y, ( MoveCondition('squareNotUnderAttack', 0, 0), MoveCondition('notPreviouslyCastled', 0, 0), MoveCondition('selfNeverMoved', 0, 0), MoveCondition('closestRookToRightNeverMoved', 1, 0), MoveCondition('opponentNotAttacking2AdjacentRightSquares', 1, 0), MoveCondition('pieceIsWhite', 0, 0), MoveCondition('squareHasNoPiece', +1, 0), MoveCondition('squareHasNoPiece', +2, 0) ) ) )
				self.allowedCoordinates.append( ( x-2, y, ( MoveCondition('squareNotUnderAttack', 0, 0), MoveCondition('notPreviouslyCastled', 0, 0), MoveCondition('selfNeverMoved', 0, 0), MoveCondition('closestRookToLeftNeverMoved', 1, 0), MoveCondition('opponentNotAttacking2AdjacentLeftSquares', 1, 0), MoveCondition('pieceIsWhite', 0, 0), MoveCondition('squareHasNoPiece', -1, 0), MoveCondition('squareHasNoPiece', -2, 0), MoveCondition('squareHasNoPiece', -3, 0) ) ) )
				self.allowedCoordinates.append( ( x+2, y, ( MoveCondition('squareNotUnderAttack', 0, 0), MoveCondition('notPreviouslyCastled', 0, 0), MoveCondition('selfNeverMoved', 0, 0), MoveCondition('closestRookToRightNeverMoved', 1, 0), MoveCondition('opponentNotAttacking2AdjacentRightSquares', 1, 0), MoveCondition('pieceIsBlack', 0, 0), MoveCondition('squareHasNoPiece', +1, 0), MoveCondition('squareHasNoPiece', +2, 0), MoveCondition('squareHasNoPiece', +3, 0) ) ) )
				self.allowedCoordinates.append( ( x-2, y, ( MoveCondition('squareNotUnderAttack', 0, 0), MoveCondition('notPreviouslyCastled', 0, 0), MoveCondition('selfNeverMoved', 0, 0), MoveCondition('closestRookToLeftNeverMoved', 1, 0), MoveCondition('opponentNotAttacking2AdjacentLeftSquares', 1, 0), MoveCondition('pieceIsBlack', 0, 0), MoveCondition('squareHasNoPiece', -1, 0), MoveCondition('squareHasNoPiece', -2, 0) ) ) )

			elif movementType == 'diagonal_straight_kings_not_adjacent':
				x, y, d = 0, 0, distance
				#diagonal
				self.allowedCoordinates.append(( x+d, y+d, ( MoveCondition('kingsNotAdjacent', d, d), MoveCondition('squareNotUnderAttack', d, d) ) ))
				self.allowedCoordinates.append(( x+d, y-d, ( MoveCondition('kingsNotAdjacent', d, -d), MoveCondition('squareNotUnderAttack', d, -d) ) ))
				self.allowedCoordinates.append(( x-d, y+d, ( MoveCondition('kingsNotAdjacent', -d, d), MoveCondition('squareNotUnderAttack', -d, d) ) ))
				self.allowedCoordinates.append(( x-d, y-d, ( MoveCondition('kingsNotAdjacent', -d, -d), MoveCondition('squareNotUnderAttack', -d, -d) ) ))
				#streight
				self.allowedCoordinates.append(( x+d, y, ( MoveCondition('kingsNotAdjacent', d, 0), MoveCondition('squareNotUnderAttack', d, 0) ) ))
				self.allowedCoordinates.append(( x-d, y, ( MoveCondition('kingsNotAdjacent', -d, 0), MoveCondition('squareNotUnderAttack', -d, 0) ) ))
				self.allowedCoordinates.append(( x, y+d, ( MoveCondition('kingsNotAdjacent', 0, d), MoveCondition('squareNotUnderAttack', 0, d) ) ))
				self.allowedCoordinates.append(( x, y-d, ( MoveCondition('kingsNotAdjacent', 0, -d), MoveCondition('squareNotUnderAttack', 0, -d) ) ))

			elif movementType == 'knight_move':
				x, y = 0, 0
				self.allowedCoordinates.append((x+2, y+1, None))
				self.allowedCoordinates.append((x+2, y-1, None))
				self.allowedCoordinates.append((x-2, y+1, None))
				self.allowedCoordinates.append((x-2, y-1, None))
				self.allowedCoordinates.append((x+1, y+2, None))
				self.allowedCoordinates.append((x-1, y+2, None))
				self.allowedCoordinates.append((x+1, y-2, None))
				self.allowedCoordinates.append((x-1, y-2, None))

			else: print('No definition for movement type:' + movementType + ' in class MovementType')

			#make sure that in self.allowedCoordinates all elements are of type tuple( x, y, ( MoveCondition(), MoveCondition(),... ) )
			try:
				for x, y, moveConditionTupleOrNone in self.allowedCoordinates:
					assert moveConditionTupleOrNone is None or type(moveConditionTupleOrNone) == tuple, 'Piece with move rule x:'+str(x)+', y:'+str(y)+' must have allowedCoordinates[2] of type tuple or None, not of type:'+str(type(moveConditionTupleOrNone))
			except TypeError as e:
				print('Error at end of __init__() in MovementType class')
				print('Piece must have allowedCoordinates[2] of type tuple or None')
				print(e)
				raise SystemExit

			self.jumper = jumper


	#returns list of all positions reachable in one move from startSquareX, startSquareY.
	#position startSquareX, startSquareY is not returned
	#Some positions may be outside of the board (ex. (-1, 4), (0, 13),... ).
	def allowedPositions(self, startSquareX, startSquareY, startPieceisWhite):
		
		allowedPositionsList = []

		#create list of coordinates shifted by piece origin taking into account piece color (ex. pawn direction)
		for x, y, moveConditionsLst in self.allowedCoordinates:
			if startPieceisWhite:
				allowedPositionsList.append( (x+startSquareX, y+startSquareY) )
			else:
				allowedPositionsList.append( (-x+startSquareX, -y+startSquareY) )

		return allowedPositionsList


	#returns list containing lists which contain MoveCondition instances.
	#Once all conditions from any one of the inner lists have been satisfied move from startSquare to endSquare is allowed
	def conditions(self, startSquareX, startSquareY, endSquareX, endSquareY, startPieceisWhite):

		listOfMoveConditionLists = []
		if startPieceisWhite:
			for x, y, moveConditionsLst in self.allowedCoordinates:
				if x+startSquareX == endSquareX and y+startSquareY == endSquareY:
					if moveConditionsLst == None: return None
					else: listOfMoveConditionLists.append(moveConditionsLst)

		else:
			for x, y, moveConditionsLst in self.allowedCoordinates:
				if -x+startSquareX == endSquareX and -y+startSquareY == endSquareY:
					if moveConditionsLst == None: return None
					else: listOfMoveConditionLists.append(moveConditionsLst)
		
		#return list containg lists of move conditions
		#return format:(  (MoveCondition('cond1'), MoveCondition('cond2'),...), (MoveCondition('cond3'), MoveCondition('cond4'),...),...  )
		#pawn return example in case both en passan move and capture are conditionally possible:
		#return (  (MoveCondition('squareHasEnemyPiece')), (MoveCondition('pieceIsOnCertainRank'), MoveCondition('enPassantSquare'))  )
		return listOfMoveConditionLists


	#returns True if Piece is able to jump over other pieces when moving
	def isJumper(self):
		return self.jumper