import sys, os

def blockPrint():
    sys.stdout = open(os.devnull, 'w')

def enablePrint():
    sys.stdout = sys.__stdout__


from BoardManager import BoardManager

fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'


from enginesComponents.FirstLegalMove import FirstLegalMove
from enginesComponents.PlainMinimax import PlainMinimax
from enginesComponents.MinimaxWithAlphaBeta import MinimaxWithAlphaBeta

exit = False

while exit == False:

	try:
		for line in sys.stdin:

			line = line.strip()#remove '\n'

			if 'isready' in line:
				sys.stdout.write('readyok'+'\n')

			if 'exit' in line or 'quit' in line:
				exit = True

			if 'uci' in line:
				if line != 'ucinewgame':
					sys.stdout.write('uciok'+'\n')

			if 'go' in line:
				
				blockPrint()
				bm = BoardManager(8, 8, fen)
				#possible fen format:
				#position fen 6q1/5k2/8/8/8/8/PPP1K3/8 w - - 0 1 moves a2a3
				#moves a2a3 means that in '6q1/5k2/8/8/8/8/PPP1K3/8' position move a2a3 was made by white,
				#so it's actually blacks move even though 'w' is in fen string
				#code above will setup '6q1/5k2/8/8/8/8/PPP1K3/8' part of the position

				sideToMove = fen.split(' ')[1]#'w' or 'b'
				sideToMoveIsWhite = False
				if sideToMove == 'w': sideToMoveIsWhite = True

				possible3FoldRepetitionMove = ''

				#now 'moves' part will be applied
				if 'moves' in fen:

						movesString = fen.split('moves ')[1]

						if len(movesString) > 17:#4-5 chars per move with spaces in beween moves
							#move leading to before last
							possible3FoldRepetitionMove = list(reversed(movesString.split(' ')))[3]

						for move in movesString.split(' '):

							#if move not made
							if not bm.makeMove(move[:2], move[2:], sideToMoveIsWhite):
								enablePrint()
								sys.stdout.write('tellusererror: ' + 'fen string(' + fen + ') move:' + move + ' (side isWhite=' + str(sideToMoveIsWhite) + ') is not legal.' + '\n')
								blockPrint()

							#flip side to move as moves are made
							if sideToMoveIsWhite == True: sideToMoveIsWhite = False
							else: sideToMoveIsWhite = True


				#give engine current position via BoardManager
				eng = MinimaxWithAlphaBeta(bm)#eng = PlainMinimax(bm)
				#get best move for side sideToMoveIsWhite=True or False
				m = eng.move(sideToMoveIsWhite)

				eng = FirstLegalMove(bm)

				if m == '': m = eng.move(sideToMoveIsWhite)
				if m == possible3FoldRepetitionMove: m = eng.move(sideToMoveIsWhite)

				enablePrint()
				sys.stdout.write('bestmove ' + m + '\n')
			
			if 'position fen ' in line:
				fen = line.split('position fen ')[1]

			if 'position startpos' in line:

				if line == 'position startpos':
					fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
				else:
					fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 ' + line.split('position startpos ')[1]

			#communicate between 2 processes with stdin and stdout (https://stackoverflow.com/a/26464700)
			sys.stdout.flush()
			
			if exit: break
	except KeyboardInterrupt as kie:
		break