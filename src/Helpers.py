from Pieces import EmptySquare, King

class Helpers():

	def __init__(self, board):

		#this method will temporarily set class field `board` in unknown state and than delete it
		#self._unitTest_noPieceInBetween()#TURN ON TEST

		self.board = board

	#1-indexed (normally start and end square coordinates equal from 1 to 8)
	def noPieceInBetween(self, startSquareX, startSquareY, endSquareX, endSquareY):

		#same square
		if startSquareX == endSquareX and startSquareY == endSquareY: return False


		yPos = 1
		#same column
		if startSquareX == endSquareX:

			#moving down
			if startSquareY > endSquareY:
				
				while startSquareY - yPos != endSquareY:

					#no piece on startSquareX, startSquareY-yPos
					tempPiece = self.board.getPiece(startSquareX, startSquareY-yPos)
					if not isinstance(tempPiece, EmptySquare): return False

					yPos += 1

			#moving up
			else: #startSquareY < endSquareY
				
				while startSquareY + yPos != endSquareY:

					#no piece on startSquareX, startSquareY-yPos
					tempPiece = self.board.getPiece(startSquareX, startSquareY+yPos)
					if not isinstance(tempPiece, EmptySquare): return False

					yPos += 1

			return True


		xPos = 1
		#same row
		if startSquareY == endSquareY:

			#moving left
			if startSquareX > endSquareX:
				
				while startSquareX - xPos != endSquareX:

					#no piece on startSquareX, startSquareY
					tempPiece = self.board.getPiece(startSquareX-xPos, startSquareY)
					if not isinstance(tempPiece, EmptySquare): return False

					xPos += 1

			#moving right
			else: #startSquareX < endSquareX
				
				while startSquareX + xPos != endSquareX:

					#no piece on startSquareX+xPos, startSquareY
					tempPiece = self.board.getPiece(startSquareX+xPos, startSquareY)
					if not isinstance(tempPiece, EmptySquare): return False

					xPos += 1

			return True


		#diagonally distant
		if abs(startSquareX - endSquareX) == abs(startSquareY - endSquareY):

			while abs(startSquareX - endSquareX) != 1 and abs(startSquareY - endSquareY) != 1:

				#end square left of start square
				if endSquareX < startSquareX:
					endSquareX += 1

					#end square below start square
					if endSquareY < startSquareY:
						endSquareY += 1

					#end square above start square
					else:
						endSquareY -= 1

				#end square right of start square
				else:
					endSquareX -= 1

					#end square below start square
					if endSquareY < startSquareY:
						endSquareY += 1

					#end square above start square
					else:
						endSquareY -= 1
						
				tempPiece = self.board.getPiece(endSquareX, endSquareY)
				if not isinstance(tempPiece, EmptySquare): return False

			return True


		#startSquareX != endSquareX or startSquareY != endSquareY (ie. different squares)
		#and not aligned on any vertical, horizontal or diagonal line
		return False

	#this test should be run before using noPieceInBetween() because class field `board` will be overwritten by this test
	def _unitTest_noPieceInBetween(self):
		print('running', '_unitTest_noPieceInBetween')

		from Board import Board
		self.board = Board(8, 8)
		dummyPiece = King('w')
		self.board.setPiece(3, 3, dummyPiece)

		#test names must be unique otherwise they will be mutualy overwritten
		positions = {'test1': [[1, 4], [8, 4], True], \
					 'test2': [[1, 1], [3, 4], False], \
					 'test3': [[1, 1], [2, 2], True], \
					 'test4': [[1, 1], [1, 2], True], \
					 'test5': [[4, 4], [8, 8], True], \
					 'test6': [[1, 5], [1, 5], False], \
					 'test7': [[1, 6], [6, 1], True], \
					 'test8': [[1, 6], [6, 1], True], \
					 'test9': [[1, 6], [4, 4], False]}

		for testName, data in positions.items():
			startSquare, endSquare, expectedOutcome = data[0], data[1], data[2]

			startSquareX, startSquareY, endSquareX, endSquareY = startSquare[0], startSquare[1], endSquare[0], endSquare[1]
			assert self.noPieceInBetween(startSquareX, startSquareY, endSquareX, endSquareY) == expectedOutcome, 'failed unit test:' + testName + ' for noPieceInBetween()'

		for testName, data in positions.items():
			endSquare, startSquare, expectedOutcome = data[0], data[1], data[2]

			startSquareX, startSquareY, endSquareX, endSquareY = startSquare[0], startSquare[1], endSquare[0], endSquare[1]
			assert self.noPieceInBetween(startSquareX, startSquareY, endSquareX, endSquareY) == expectedOutcome, 'failed unit test: reversed_coordinates_' + testName + ' for noPieceInBetween()'

		del self.board
		print('finished', '_unitTest_noPieceInBetween\n')
	
	#find first instance of pieceType on the board to left or right from startSquareX, startSquareY (looking relative to the board (white perspective))
	#returns tuple. tuple[0]=None if there are no pieces of Type pieceType to desired side;
	#if there are returns first instance of that piece and its x,y coordinates in a tuple
	#will work for startSquareX that is just outside of the board (normally: startSquareX=0, startSquareX=9)
	#checking starts from square adjacent to startSquareX
	def getFirstPieceToSide(self, startSquareX, startSquareY, pieceType, rightSide):

		x = 0
		if rightSide:
			x = startSquareX + 1
		else:
			x = startSquareX - 1

		while x >= 1 and x <= self.board.boardXSize:#while not outside of the board
			
			piece = self.board.getPiece(x, startSquareY)
			if isinstance(piece, pieceType): return (piece, x, startSquareY)

			if rightSide:
				x += 1
			else:
				x -= 1

		return (None, x, startSquareY)

	def getKingXY(self, isWhite):

		for y in range(1, self.board.boardYSize + 1):

			for x in range(1, self.board.boardXSize + 1):

				if isinstance(self.board.getPiece(x, y), King):

					if self.board.getPiece(x, y).isWhite() == isWhite:
						return (x, y)

		print('No king on the board (side: white==' + str(isWhite) + ').')
		return (1, 1)