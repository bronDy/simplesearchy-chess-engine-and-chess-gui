from Pieces import King
from Board import Board
from BoardManager import BoardManager

from FEN import FEN

import sys, os
import subprocess

#TODO add 50 move rule and 3 fold repetition
#piecesSetup may be fen string as long it doesn't have moves part
def main(player1, player2, piecesSetup = 'standard'):
	print('Game start')
	movesMade = ''#records all moves made in current game to show to player when game completes

	exitCommands = ('q', 'Q', 'quit', 'QUIT', 'exit', 'EXIT')

	#setup board    
	sizeX = 8
	sizeY = 8
    
	bm = BoardManager(sizeX, sizeY, piecesSetup)

	bm.displayBoard()


	toSquare = 'destinationCoordinates'
	while True:


		print('-------> White move <-------')
		if bm.inCheckmate(True) in (True, 'stalemate'):
			if bm.inCheckmate(True) == True:
				print('White in checkmate.')
			else:
				print('Game is a draw.')
			break

		moveNotMade = True
		while moveNotMade:

			fromSquare, toSquare = player1.move( str(bm.board), bm.gameRecord, True )
			if toSquare in exitCommands or fromSquare in exitCommands: return
			
			if bm.makeMove(fromSquare, toSquare, True): moveNotMade = False

		movesMade += fromSquare + toSquare + ' '
		print()
		bm.displayBoard()


		print('-------> Black move <-------')
		if bm.inCheckmate(False) in (True, 'stalemate'):
			if bm.inCheckmate(False) == True:
				print('Black in checkmate.')
			else:
				print('Game is a draw.')
			break

		moveNotMade = True
		while moveNotMade:

			fromSquare, toSquare = player2.move( str(bm.board), bm.gameRecord, False )
			if toSquare in exitCommands or fromSquare in exitCommands: return
			
			if bm.makeMove(fromSquare, toSquare, False): moveNotMade = False

		movesMade += fromSquare + toSquare + ' '
		print()
		bm.displayBoard()


	print('Moves made:' + movesMade)
	print('Game end')


class Player():

	def move(self, position, gameRecord, isWhite): print('Player should be overwriten')

class Human(Player):

	def move(self, position, gameRecord, isWhite):

		try:
			fromSquare = input('\nEnter starting square:')
			toSquare = input('Enter destination square:')
		except KeyboardInterrupt: return ('quit', 'exit')
		return (fromSquare, toSquare)

class AI(Player):

	#positionLastTime = ''#after first move positionLastTime = BoardManager().board
	#positionLastTime is used to remember position last time AI's move function was called and compare it to position on this call
	#if the positions are same twice in a row it means that on last call to move
	#AI returned an invalid set of coordinates(fromSquare, toSquare)
	#in this case program should be stopped to avoid infinite loop

	#engine = Popen()

	#startingPosition examples('startpos', 'fen 4KP2/8/8/8/4kp2/8/8/8', 'fen 6q1/5k2/8/8/8/8/PPP1K3/8 w - - 0 1 moves a2a3')
	def __init__(self, engineName, startingPosition = 'startpos'):

		engine = subprocess.Popen(
			engineName,
			universal_newlines=True,
			shell=True,
			stdin=subprocess.PIPE,
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			bufsize=1
		)

		#FIXME:2 engine procs are started at this point for some reason. It's not a big problem because they both later terminate

		sendToEngine(engine, '', True)#isready
		sendToEngine(engine, 'uci', True)
		#sendToEngine(engine, 'setoption name Hash value 128', True)
		sendToEngine(engine, 'ucinewgame', True)
		sendToEngine(engine, 'position ' + startingPosition, True)

		self.engine = engine
		
		self.positionLastTime = ''

		self.fen = FEN()

	def move(self, position, gameRecord, isWhite):

		FENPosition = self.fen.getFENFromBoard(position, gameRecord, isWhite)
		fromSquare, toSquare = self.getMoveFromPosition(FENPosition)

		if self.positionLastTime == position:
			try:
				self.engine.stdin.write('exit'+'\n')
				self.engine.wait()
			finally:
				self.engine.kill()

		assert self.positionLastTime != position, 'AI failed to make a valid move in position:\n' + self.positionLastTime
		self.positionLastTime = position

		return (fromSquare, toSquare)

	def getMoveFromPosition(self, FENPosition):

		sendToEngine(self.engine, 'position fen '+FENPosition, True)
		response = sendToEngine(self.engine, 'go', False)

		fromSquare, toSquare = 'a1', 'a1'#in case engine can't find a legal move

		if 'bestmove' in response:
			x = response.split('bestmove ')
			bestMove = x[1].split(' ')[0]

			fromSquare, toSquare = bestMove[:2], bestMove[2:]

		return (fromSquare, toSquare)

	def __exit__(self, exc_type, exc_val, exc_tb):#__del__(self)
		try:
			self.engine.stdin.write('exit'+'\n')
			#self.engine.wait()
			self.engine.kill()
		finally:
			pass#self.engine.kill()

def sendToEngine(engine, command, sendIsReadyAfterCommand):
	#print('\ngui:\n\t'+command)#debug
	engine.stdin.write(command+'\n')

	# using the 'isready' command (engine has to answer 'readyok')
	# to indicate current last line of stdout
	if sendIsReadyAfterCommand: engine.stdin.write('isready\n')
	#print('\nengine:')#debug

	engineResponse = ''
	while True:
		text = engine.stdout.readline().strip()
		engineResponse += text
		#if text != '':#debug
		#	print('\t'+text)#debug
		if 'readyok' in text: break
		if 'bestmove' in text: break

	return engineResponse

if __name__ == '__main__':
	engineName = 'SimpleSearchy.exe'#default engine
	while True:
		engineName = 'SimpleSearchy.exe'#default engine
		guiVersion = 1.0
		print('SimpleGui v' + str(guiVersion))

		if input('\nType `help` for help or press any key to continue:') == 'help':
			print('\nType `exit` to exit the program.')
			print('\nSecond argument (argv[1]) is used when calling this gui to select game engine used.')
			print('Gui will search in same directory for file with name argv[1] and comunicate with it using UCI.')
			print('If no second command line argument is provided this gui will use default engine:', engineName, '\n')
			print('User should enter moves in format `from square`, `to square`.')
			print('Example moves:  opening move:e2, e4  promotion into rook:e7, e8r  short castle:e1, g1')
			print('Example starting position:6k2/8/6K2/8/8/8/8/R7 w - -')
			print('Supported commands [uci,isready,position startpos,position fen,go,exit,quit]')
			if input('') in ('q', 'Q', 'quit', 'QUIT', 'exit', 'EXIT'): break

		if len(sys.argv) > 1:

			print('\nSetting', sys.argv[1], 'as engine.\n')
			engineName = sys.argv[1]

		else: print('\nSetting', engineName, 'as engine (default engine).\n')

		#select players
		player1 = None
		player2 = None
		if input('player1 is human (y/n):') in ('n', 'N', 'no', 'NO', '2'):  player1 = AI(engineName)
		else: player1 = Human()
		if input('player2 is human (y/n):') in ('y', 'Y', 'yes', 'YES', '1'): player2 = Human()
		else: player2 = AI(engineName)

		#select starting position
		startingPosition = input('Enter starting position fen or press enter to use default:')
		if startingPosition in (None, '', ' ', 'default'): main(player1, player2)
		else: main(player1, player2, startingPosition)

		try:
			if input('Do you want to exit program (y/n):') in ('y', 'Y', 'yes', 'YES'): break
		except KeyboardInterrupt: break