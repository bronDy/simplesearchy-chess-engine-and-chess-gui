#### SimpleSearchy uses minimax search with alpha–beta pruning to find the best move. It's depth ranges from 1 to 2 moves ahead.  It will take 1 to 10 seconds to find the move depending on number of pieces on the board

```
  +---+---+---+---+---+---+---+---+
8 | r | n | b | q | k | b | n | r |
  +---+---+---+---+---+---+---+---+
7 | p | p | p | p | p | p | p | p |
  +---+---+---+---+---+---+---+---+
6 |   |   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+
5 |   |   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+
4 |   |   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+
3 |   |   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+
2 | P | P | P | P | P | P | P | P |
  +---+---+---+---+---+---+---+---+
1 | R | N | B | Q | K | B | N | R |
  +---+---+---+---+---+---+---+---+
    a   b   c   d   e   f   g   h
```

**To run simply download `exe` folder.**

```
Simple Gui 1.0.exe and main.py must have in the same folder SimpleSearchy.exe or
another engine whose name would be provided as command line argument (ex. >main.py stockfish_10_x64.exe)
```


**uci communication to SimpleSearchy.exe:**

```
position fen rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
go
```

*note: SimpleSearchy doesn't accept uci stop command

**Compiling:**
```
pyinstaller.exe --onefile "SimpleSearchy.py" -F
pyinstaller.exe --onefile "main.py" -F
```